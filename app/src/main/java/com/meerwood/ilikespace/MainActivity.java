package com.meerwood.ilikespace;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
//import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.meerwood.ilikespace.api.NasaApi;
import com.meerwood.ilikespace.model.NasaPotdModel;
import com.meerwood.ilikespace.model.rover.Photo;
import com.meerwood.ilikespace.model.rover.RoverPictures;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;


public class MainActivity extends AppCompatActivity
    implements GridImageViewFragment.OnFragmentInteractionListener{

    //Log tag for debugging/errors
    private final String LOG_TAG = MainActivity.class.getSimpleName();

    //GridImageAdapters to handle the different types of information
    PotdGridImageAdapter mPotdGridImageAdapter;
    RoverGridImageAdapter mRoverGridImageAdapter;

    //Calendar offset used in return images from Image of the Day
    int calendarOffset;
    OkHttpClient okHttpClient;
    Retrofit retrofit;
    NasaApi nasaApi;

    //All this is used for the slid out menu.
    ListView mDrawerList;
    RelativeLayout mDrawerPane;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Add a list of items to the navmenu.
        mNavItems.add(new NavItem("Picture of the Day", "NASA's latest pictures of the day", R.drawable.ic_image));
        mNavItems.add( new NavItem("Curiosity", "Latest photos from Curiosity", R.drawable.ic_projector));
        mNavItems.add( new NavItem("Near Earth Objects", "Details about the closest comets", R.drawable.ic_cube_send));

        //Caputre the different elements of the slide out drawer.
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);
        mDrawerList = (ListView) findViewById(R.id.navList);

        //Set the adapter for the drawer list.
        DrawerListAdapter adapter = new DrawerListAdapter(this, mNavItems);
        mDrawerList.setAdapter(adapter);

        //Set the action for when a menu item is pressed.
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItemFromDrawer(position);
            }
        });

        //Capture the toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //This binds the gridview to an adapter and sets the on click ability.
        final GridView gridview = (GridView) findViewById(R.id.gridview);

        mPotdGridImageAdapter = new PotdGridImageAdapter(
                this,
                R.layout.grid_item,
                R.id.grid_item,
                new ArrayList<NasaPotdModel>());

        mRoverGridImageAdapter = new RoverGridImageAdapter(
                this,
                R.layout.grid_item,
                R.id.grid_item,
                new ArrayList<Photo>());

        okHttpClient = createCachedClient(this);

        retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(getString(R.string.api_base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        nasaApi = retrofit.create(NasaApi.class);

        gridview.setAdapter(mPotdGridImageAdapter);
        //Due to being a day ahead of the NASA server an error would occur anytime
        //the app requested results for today. Dodgy fix to stop this
        calendarOffset = 1; //TODO fix calendar offset to take current timezone into account.
        loadNextTen();



        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                ListAdapter current = gridview.getAdapter();
                if(current instanceof PotdGridImageAdapter){
                    Intent intent = new Intent(getApplicationContext(), ViewStarsActivity.class);
                    NasaPotdModel potd = (NasaPotdModel) parent.getItemAtPosition(position);
                    intent.putExtra("potd", potd);
                    startActivity(intent);
                    Toast.makeText(MainActivity.this, mPotdGridImageAdapter.getItem(position).getTitle(),
                            Toast.LENGTH_SHORT).show();
                }

            }
        });

        // TODO: 21/01/16 Possibly add dynamic retrieval when reaching the bottom of the results.
        /*gridview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(firstVisibleItem + visibleItemCount == totalItemCount
                        && totalItemCount != 0
                        && totalItemCount < 20
                        && flagLoading == false){
                            flagLoading = true;
                            loadNextTen();

                }
            }
        });*/



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //This will use Retrofit to fetch the api results of the given
    //date and then send the api to the imageAdapter, who will then
    //load the image.
    public void fetchImages(){

        //Calendar used for the date in the api request.
        Calendar offSetCal = Calendar.getInstance();
        offSetCal.add(Calendar.DAY_OF_YEAR, (-1 * calendarOffset));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = offSetCal.getTime();

        //A call telling it to use getPotdWithDate.
        Call<NasaPotdModel> call = nasaApi.getPotdWithDate(sdf.format(date),
                BuildConfig.NASA_API_KEY);

        //Async call.
        call.enqueue(new Callback<NasaPotdModel>() {
            //If success, add it to mPotdGridImageAdapter
            @Override
            public void onResponse(Response<NasaPotdModel> response) {
                if (response.isSuccess()) {
                    mPotdGridImageAdapter.add(response.body());
                } else {
                    try {
                        Log.e(LOG_TAG, "Incorrect response from api: " + response.errorBody().string());
                    }catch (IOException ex){
                        Log.e(LOG_TAG, "Total fuck up.");
                    }
                }
            }

            //If no internet, throw up a toast saying no interwebs.
            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(MainActivity.this, "" + "Did not connect, using cache.", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void loadNextTen(){
        for (int i = 0; i < 10; i++) {
            fetchImages();
            calendarOffset += 1;
        }
    }


    private OkHttpClient createCachedClient(final Context context) {
        File httpCacheDirectory = new File(context.getCacheDir(), "cache_file");

        Cache cache = new Cache(httpCacheDirectory, 20 * 1024 * 1024);
        OkHttpClient okHttpClient = new OkHttpClient()
                .newBuilder()
                .cache(cache)
                .addInterceptor(
                        new Interceptor() {
                            @Override
                            public okhttp3.Response intercept(Chain chain) throws IOException {
                                Request originalRequest = chain.request();
                                String cacheHeaderValue = getOnline(context)
                                        ? "public, max-age=2419200"
                                        : "public, only-if-cached, max-stale=2419200";
                                Request request = originalRequest.newBuilder().build();
                                okhttp3.Response response = chain.proceed(request);
                                return response.newBuilder()
                                        .removeHeader("Pragma")
                                        .removeHeader("Cache-Control")
                                        .header("Cache-Control", cacheHeaderValue)
                                        .build();
                            }
                        }
                )
                .addNetworkInterceptor(
                        new Interceptor() {
                            @Override
                            public okhttp3.Response intercept(Chain chain) throws IOException {
                                Request originalRequest = chain.request();
                                String cacheHeaderValue = getOnline(context)
                                        ? "public, max-age=2419200"
                                        : "public, only-if-cached, max-stale=2419200";
                                Request request = originalRequest.newBuilder().build();
                                okhttp3.Response response = chain.proceed(request);
                                return response.newBuilder()
                                        .removeHeader("Pragma")
                                        .removeHeader("Cache-Control")
                                        .header("Cache-Control", cacheHeaderValue)
                                        .build();
                            }
                        }
                ).build();
                return okHttpClient;
    }

    private boolean getOnline(Context context){
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        Toast.makeText(getApplicationContext(),"We made toast",Toast.LENGTH_LONG).show();
    }

    class NavItem {
        String mTitle;
        String mSubtitle;
        int mIcon;

        public NavItem(String title, String subtitle, int icon) {
            mTitle = title;
            mSubtitle = subtitle;
            mIcon = icon;
        }
    }

    class DrawerListAdapter extends BaseAdapter {
        Context mContext;
        ArrayList<NavItem> mNavItems;

        public DrawerListAdapter(Context context, ArrayList<NavItem> navItems){
            mContext = context;
            mNavItems = navItems;
        }

        @Override
        public int getCount() {
            return mNavItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mNavItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.drawer_item, null);
            } else {
                view = convertView;
            }

            TextView titleView = (TextView) view.findViewById(R.id.title);
            TextView subtitleView = (TextView) view.findViewById(R.id.subtitle);
            ImageView iconView = (ImageView) view.findViewById(R.id.icon);

            titleView.setText( mNavItems.get(position).mTitle );
            subtitleView.setText(mNavItems.get(position).mSubtitle);
            iconView.setImageResource( mNavItems.get(position).mIcon);

            return view;
        }
    }

    private void selectItemFromDrawer(int position) {
        /*
        Fragment fragment = new PreferencesFragment();
        FragmentManager fm = getFragmentManager();
        Fragment current = fm.findFragmentById(R.id.gridview_holder);
        fm.beginTransaction()
                .remove(current)
                .commit();
        */
        GridView gridView = (GridView) findViewById(R.id.gridview);
        mRoverGridImageAdapter.clear();
        mPotdGridImageAdapter.clear();
        switch (position){
            case 0:
                calendarOffset = 1;
                gridView.setAdapter(mPotdGridImageAdapter);
                loadNextTen();
                break;
            case 1:
                gridView.setAdapter(mRoverGridImageAdapter);
                loadRoverPhotos();
                break;
        }
        mDrawerList.setItemChecked(position, true);
        setTitle(mNavItems.get(position).mTitle);

        mDrawerLayout.closeDrawer(mDrawerPane);
    }

    private void loadRoverPhotos() {
        //Calendar used for the date in the api request.
        Calendar offSetCal = Calendar.getInstance();
        offSetCal.add(Calendar.DAY_OF_YEAR, -1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = offSetCal.getTime();

        //A call telling it to use getPotdWithDate.
        Call<RoverPictures> callFrontCam = nasaApi.getRoverPictures("curiosity", sdf.format(date),
                "fhaz", BuildConfig.NASA_API_KEY);

        //Async call.
        callFrontCam.enqueue(new Callback<RoverPictures>() {
            //If success, add it to mPotdGridImageAdapter
            @Override
            public void onResponse(Response<RoverPictures> response) {
                if (response.isSuccess()) {
                    mRoverGridImageAdapter.addAll(response.body().getPhotos());
                } else {
                    try {
                        Log.e(LOG_TAG, "Incorrect response from api: " + response.errorBody().string());
                    }catch (IOException ex){
                        Log.e(LOG_TAG, "Total fuck up.");
                    }
                }
            }

            //If no internet, throw up a toast saying no interwebs.
            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(MainActivity.this, "" + "Did not connect, using cache.", Toast.LENGTH_SHORT).show();
            }
        });

        Call<RoverPictures> callRearCam = nasaApi.getRoverPictures("curiosity", sdf.format(date),
                "rhaz" ,BuildConfig.NASA_API_KEY);

        //Async call.
        callRearCam.enqueue(new Callback<RoverPictures>() {
            //If success, add it to mPotdGridImageAdapter
            @Override
            public void onResponse(Response<RoverPictures> response) {
                if (response.isSuccess()) {
                    mRoverGridImageAdapter.addAll(response.body().getPhotos());
                } else {
                    try {
                        Log.e(LOG_TAG, "Incorrect response from api: " + response.errorBody().string());
                    }catch (IOException ex){
                        Log.e(LOG_TAG, "Total fuck up.");
                    }
                }
            }

            //If no internet, throw up a toast saying no interwebs.
            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(MainActivity.this, "" + "Did not connect, using cache.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
