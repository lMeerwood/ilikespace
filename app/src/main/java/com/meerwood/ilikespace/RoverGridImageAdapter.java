package com.meerwood.ilikespace;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.meerwood.ilikespace.model.NasaPotdModel;
import com.meerwood.ilikespace.model.rover.Photo;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by lenny on 19/01/16.
 * PotdGridImageAdapter is an extension of the ArrayAdapter.
 * It's designed to take a NasaPotdModel and show
 * A square picture with the title and date over
 * laid.
 */
public class RoverGridImageAdapter extends ArrayAdapter<Photo> {
    private final String LOG_TAG = RoverGridImageAdapter.class.getSimpleName();
    private Context mContext;
    private final LayoutInflater mInflater;
    private ArrayList<Photo> photos; //An array to hold the photos.

    //Default Constructor.
    public RoverGridImageAdapter(Context c, int resource, int textViewResourceId,
                                 ArrayList<Photo> objects) {
        super(c, resource, textViewResourceId, objects);
        mContext = c;
        mInflater = LayoutInflater.from(c);
        photos = objects;

    }

    //Return the count
    public int getCount() {
        return photos.size();
    }

    //Return a specific potd
    public Photo getItem(int position) {
        return photos.get(position);
    }

    //no fucking clue.
    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View v, ViewGroup parent) {

        //Objects to hold the imageview and textview
        final ImageView imageView;
        TextView name;

        //Because the views are recycled, this will only run
        //if the view is null. If it is, it will inflate the
        //view.
        if (v == null) {
            v = mInflater.inflate(R.layout.grid_item, parent, false);
            v.setTag(R.id.grid_picture, v.findViewById(R.id.grid_picture));
            v.setTag(R.id.grid_text, v.findViewById(R.id.grid_text));
        }

        //Link the actualy imageview and textview to their objects.
        imageView = (ImageView) v.getTag(R.id.grid_picture);
        name = (TextView) v.getTag(R.id.grid_text);

        //This will try to load the image from the nasa website. It will also
        //add the title and date to the text field.
        try {
            Picasso.with(mContext)
                    .load(photos.get(position).getImgSrc())
                    .placeholder(R.drawable.nasa)
                    .error(R.drawable.sample_0)
                    .into(imageView);

            //name.setText(photos.get(position).getId());
        } catch (NullPointerException ex) {
            //Sometimes something goes wrong. Haven't figured out what yet.
            name.setText("This image is broken. If you see a picture it has not been recycled.");
            Log.e(LOG_TAG, "Problem entry: " + position + " of " + photos.size());
        }
        return v;
    }

}