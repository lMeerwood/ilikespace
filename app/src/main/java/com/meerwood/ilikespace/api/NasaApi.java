package com.meerwood.ilikespace.api;

import com.meerwood.ilikespace.model.NasaPotdModel;
import com.meerwood.ilikespace.model.rover.Rover;
import com.meerwood.ilikespace.model.rover.RoverPictures;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by lenny on 19/01/16.
 */
public interface NasaApi {

    //Call to get Picture of the Day without a date, will return latest image.
    @GET("/planetary/apod")
    Call<NasaPotdModel> getPotd(@Query("api_key") String apikey);

    //Call to get Picture of the Day with a date, will return image from specified date.
    @GET("/planetary/apod")
    Call<NasaPotdModel> getPotdWithDate(@Query("date") String date, @Query("api_key") String apikey);

    //Call to get pictures from Mars rover, will return a list of images from the specified date.
    @GET("/mars-photos/api/v1/rovers/{rover}/photos")
    Call<RoverPictures> getRoverPictures(@Path("rover") String rover, @Query("earth_date") String date,
                                         @Query("camera") String camera ,@Query("api_key") String apikey);
}
