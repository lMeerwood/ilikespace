package com.meerwood.ilikespace;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.meerwood.ilikespace.model.NasaPotdModel;
import com.squareup.picasso.Picasso;

public class ViewStarsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_stars);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NasaPotdModel potd = (NasaPotdModel) getIntent().getSerializableExtra("potd");
        toolbar.setTitle(potd.getTitle());
        ImageView imageView = (ImageView) findViewById(R.id.imageView_star);
        Picasso.with(getApplicationContext())
                .load(potd.getUrl())
                .placeholder(R.drawable.nasa)
                .error(R.drawable.sample_0)
                .noFade()
                .into(imageView);
    }

}
